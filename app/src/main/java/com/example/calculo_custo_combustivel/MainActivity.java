package com.example.calculo_custo_combustivel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText edDistance, edConsume, edPriceLitre;
    SeekBar sbPeople;
    TextView tvAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edDistance = (EditText) findViewById(R.id.txtDistance);
        edConsume = (EditText) findViewById(R.id.txtAverageConsume);
        edPriceLitre = (EditText) findViewById(R.id.txtLitreCost);

        sbPeople = (SeekBar) findViewById(R.id.barPeople);

        tvAnswer = (TextView) findViewById(R.id.tvCost);
    }

    public void calculate(View v) {
        Double distance = Double.parseDouble(edDistance.getText().toString());
        Double consume = Double.parseDouble(edConsume.getText().toString());
        Double priceLitre = Double.parseDouble(edPriceLitre.getText().toString());

        Integer people = sbPeople.getProgress();

        Double valuePerPerson = ((distance/consume) * priceLitre) / people * 1.1;

        tvAnswer.setText(String.format("Value per person: $%.2f", valuePerPerson));
    }
}